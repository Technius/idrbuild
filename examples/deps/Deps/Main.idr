module Deps.Main

import Effects
import Effect.StdIO

program : Eff () [STDIO]
program = putStrLn "Hello world!"

main : IO ()
main = run program
