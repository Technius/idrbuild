module Idrbuild.GitWrapper

import Effects
import Effect.StdIO
import Effect.System

%default total

export
gitStatus : Eff Int [SYSTEM]
gitStatus = system "git status"

export
gitClone : String -> String -> String -> Eff Int [SYSTEM]
gitClone url dir name = system $ "git clone -q " ++ url ++ " " ++ (dir ++ "/" ++ name)
