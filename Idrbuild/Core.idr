module Idrbuild.Core

import Effects
import Effect.File
import Effect.StdIO
import Effect.System

import Idrbuild.GitWrapper

%default total

||| Downloads the idris package from the given GitHub repository into the given
||| directory.
export
downloadPackage : String -> String -> String -> Eff Bool [SYSTEM]
downloadPackage dir name repo = do
  statusCode <- gitClone ("https://github.com/" ++ repo ++ ".git") dir name
  pure (statusCode == 0)

||| Checks if idris is installed (or available from the command line).
export
checkIdrisInstallation : Eff Bool [SYSTEM]
checkIdrisInstallation = do
  status <- system "idris -v"
  pure (status == 0)

||| Defines build settings for a package
public export
record BuildSettings where
  constructor MkBuildSettings
  packageName : String
  executable : Maybe (String, String)
  dependencies : List String
  modules : List String
  idrisOptions : Maybe String

export
defaultBuildSettings : String -> BuildSettings
defaultBuildSettings pkgName =
  MkBuildSettings pkgName Nothing [] [] Nothing

filterNonEmpty : List String -> List String
filterNonEmpty = filter (\l => length l > 0)

key : String -> String -> String
key name value =
  if length value > 0
    then (name ++ " = " ++ value)
    else ""

export
generatePackageFile : BuildSettings -> String
generatePackageFile p =
  let pkgDeps = unwords $ dependencies p
      addOpts = maybe "" show (idrisOptions p)
      (exe, main) = fromMaybe ("", "") (executable p)
      modlist = modules p
      mods = unwords $ modlist ++ (
        -- Add main to modules if it's not there
        if main /= "" && not (elem main modlist)
          then [main]
          else [])
  in unlines $ filterNonEmpty [
    "package " ++ (packageName p),
    key "opts" addOpts,
    key "pkgs" pkgDeps,
    key "modules" mods,
    key "executable" exe,
    key "main" main
    ]

export
buildProject : String -> BuildSettings -> Eff Bool [FILE (), SYSTEM]
buildProject dir settings = do
  let pkgFileContents = generatePackageFile settings
  let pkgFilePath = dir ++ "/package.ipkg"
  writeStatus <- writeFile pkgFilePath pkgFileContents
  case writeStatus of
    Success => do
      status <- system $ "idris --build " ++ pkgFilePath
      pure (status == 0)
    _ => pure False
