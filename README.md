# Idrbuild

Idrbuild is a package manager and build tool
for [Idris](https://idris-lang.org/). At the moment, it only provides support
for installing packages hosted on GitHub.

## Usage

```plain
idrbuild install <name of package> <repository>
```

Example:

```plain
idrbuild install tomladris eklavya/tomladris
```

## License

Idrbuild is licensed under the MIT License.
