PKG_NAME := idrbuild
EXE_NAME := idrbuild

IDRIS := idris
IDRIS_OPTS := --ibcsubdir target

.PHONY: compile
compile: target/$(EXE_NAME)

.PHONY: run
run: compile
	@cd target && ./$(EXE_NAME)

target:
	@mkdir -p target

target/$(EXE_NAME): depends target
	@echo "Compiling..."
	@$(IDRIS) $(IDRIS_OPTS) --build $(PKG_NAME).ipkg
	@mv $(EXE_NAME) target/$(EXE_NAME)

.PHONY: install
install: depends target
	@echo "Compiling and installing..."
	@$(IDRIS) $(IDRIS_OPTS) --install $(PKG_NAME).ipkg

.PHONY: clean
clean:
	@rm -rf target
	@$(IDRIS) $(IDRIS_OPTS) --clean $(PKG_NAME).ipkg
	@make -C examples clean

.PHONY: depends
depends: lib/idris-config

lib:
	@mkdir -p lib

lib/idris-config: lib
	@[ ! -d $@ ] && git clone git@github.com:jfdm/idris-config $@ || true
	@cd $@ && sh fetch-deps.sh && make install

examples: target/$(EXE_NAME)
	@echo "Building examples..."
	@make -C examples IDRBUILD=$(PWD)/target/$(EXE_NAME)
