module Idrbuild.Parser

import Config.YAML
import Effects
import Effect.File
import Effect.Exception

import Idrbuild.Core

%default total

||| Represents an error that concerns the configuration of the build.
export
data BuildFileError : Type where
  ||| The value of a key has an incorrect type.
  ||| @ key The key
  ||| @ expected The expected type
  ||| @ actual The actual type
  InvalidType : (key: String) -> (expected: String) -> (actual: String) -> BuildFileError
  ||| A required key is missing from the build configuration.
  MissingKey : String -> BuildFileError

||| Represents an error that may be encountered when parsing a build file.
export
data BuildParserError : Type where
  ||| The build file is not a valid YAML file
  ParseError : ConfigError -> BuildParserError
  ||| The build file is incorrectly configured.
  InvalidBuild : (reason: BuildFileError) -> BuildParserError

export
Show BuildFileError where
  show (InvalidType key exp act) =
    key ++ " has invalid type. Expected: " ++ (show exp) ++ "; actual: " ++ (show act)
  show (MissingKey key) = "Missing key: " ++ key

export
Show BuildParserError where
  show (ParseError err) = "Invalid YAML file: " ++ (show err)
  show (InvalidBuild reasons) = show reasons

mapYAMLTypeString : YAMLNode -> String
mapYAMLTypeString (YAMLString _) = "String"
mapYAMLTypeString (YAMLInt _) = "Int"
mapYAMLTypeString (YAMLFloat _) = "Double"
mapYAMLTypeString (YAMLBool _) = "Bool"
mapYAMLTypeString (YAMLSeq _) = "List YAMLNode"
mapYAMLTypeString (YAMLMap _) = "List (YAMLNode, YAMLNode)"
mapYAMLTypeString YAMLNull = "null"
mapYAMLTypeString (YAMLScalar _) = "String"
mapYAMLTypeString (YAMLDoc _ _) = "List (YAMLNode, YAMLNode)"

asString : YAMLNode -> Maybe String
asString (YAMLString s) = Just s
asString _ = Nothing

asListString : YAMLNode -> Maybe (List String)
asListString (YAMLSeq l) = sequence $ [asString x | x <- l]
asListString _ = Nothing

asDict : YAMLNode -> Maybe (List (String, YAMLNode))
asDict (YAMLMap l) = sequence $ [map (\k2 => (k2, v)) (asString k) | (k, v) <- l]
asDict _ = Nothing

yamlInvalidType : String -> String -> YAMLNode -> Either BuildFileError a
yamlInvalidType key exp node = Left $ InvalidType key exp (mapYAMLTypeString node)

nodeLookup : String -> YAMLNode -> Either BuildFileError YAMLNode
nodeLookup key (YAMLMap l) = case find (\(k,_) => asString k == Just key) l of
  Just (_, v) => Right v
  Nothing => Left (MissingKey key)
nodeLookup key (YAMLDoc _ m) = nodeLookup key m
nodeLookup key node = yamlInvalidType key "List (YAMLNode, YAMLNode)" node

||| Lookup a value from a yaml node
genericLookup : String -> (YAMLNode -> Maybe a) -> String -> YAMLNode -> Either BuildFileError a
genericLookup expTy f key map = (nodeLookup key map) >>= (\x =>
  maybe (yamlInvalidType key expTy x) Right (f x))

stringLookup : String -> YAMLNode -> Either BuildFileError String
stringLookup = genericLookup "String" asString

listLookup : String -> YAMLNode -> Either BuildFileError (List String)
listLookup = genericLookup "List String" asListString

dictLookup : String -> YAMLNode -> Either BuildFileError (List (String, YAMLNode))
dictLookup = genericLookup "List (String, YAMLNode)" asDict

parseBuildFile : YAMLNode -> Either BuildFileError BuildSettings
parseBuildFile yaml = do
    pkgName <- stringLookup "name" yaml
    mods <- listLookup "modules" yaml
    let execNode = nodeLookup "executable" yaml
    let execName = execNode >>= stringLookup "name"
    let execMain = execNode >>= stringLookup "main"
    let exec = liftA2 (\n, m => (n, m)) execName execMain
    let settings = defaultBuildSettings pkgName
    let deps = listLookup "depends" yaml
    Right $ record {
      modules = mods,
      executable = eitherDefault Nothing exec,
      dependencies = either (\_ => []) id deps
    } settings
  where
    ||| Convert Either to any applicative
    eitherDefault : Applicative f => Lazy (f b) -> Either a b -> f b
    eitherDefault def e = either (\_ => Force def) pure e

||| Attempts to read the given build file.
export
partial -- readYAMLConfig is not total
readBuildFile : String -> Eff (Either BuildParserError BuildSettings) [FILE ()]
readBuildFile fileName = do
    parseResult <- readYAMLConfig fileName
    pure $ handleResult parseResult
  where
    handleResult : Either ConfigError YAMLNode -> Either BuildParserError BuildSettings
    handleResult (Left err) = Left $ ParseError err
    handleResult (Right yaml) with (parseBuildFile yaml)
      | Left err = Left $ InvalidBuild err
      | Right s = Right s
