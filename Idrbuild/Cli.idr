module Idrbuild.Main

import Effects
import Effect.StdIO
import Effect.System
import Effect.File

import Idrbuild.Core
import Idrbuild.Parser

total
usage : String -> String -> Eff () [STDIO]
usage arg0 msg = putStrLn $ "Usage: " ++ arg0 ++ " " ++ msg

total
getCwd : Eff String [SYSTEM]
getCwd = do
  pwd <- getEnv "pwd"
  pure (fromMaybe "." pwd)

total
installCmd : String -> List String -> Eff () [SYSTEM, STDIO]
installCmd _ (pkgName :: repoName :: rest) = do
  dir <- getCwd
  successful <- downloadPackage dir pkgName repoName
  if successful
    then putStrLn ("Package \"" ++ pkgName ++ "\" installed.")
    else putStrLn "Failed to download package"
installCmd arg0 _ = usage arg0 "install <package> <repo> [directory]"

buildCmd : String -> List String -> Eff () [SYSTEM, STDIO, FILE ()]
buildCmd arg0 params = do
  Right settings <- readBuildFile "build.yml" | Left err => putStrLn (show err)
  wd <- getCwd
  buildProject wd settings
  pure ()

runCmd : String -> List String -> Eff () [SYSTEM, STDIO, FILE ()]
runCmd arg0 params = do
  Right settings <- readBuildFile "build.yml" | Left err => putStrLn (show err)
  Just (name, _) <- pure (executable settings) | Nothing =>
    let msg = "No main module detected. Is build.yml configured correctly?"
    in putStrLn msg
  wd <- getCwd
  success <- buildProject wd settings
  when success $ system (wd ++ "/" ++ name) >>= (\_ => pure ())

parseArgs : Eff () [SYSTEM, STDIO, FILE ()]
parseArgs = do
  args <- getArgs
  case args of
    [arg0] => putStrLn "Try idrbuild build or idrbuild run"
    arg0 :: "install" :: params => installCmd arg0 params
    arg0 :: "build" :: params => buildCmd arg0 params
    arg0 :: "run" :: params => runCmd arg0 params
    _ => putStrLn "Unknown command."

main : IO ()
main = run parseArgs
